import 'package:flutter/material.dart';
import './lab_07.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginHeroku(),
    );
  }
}

class LoginHeroku extends StatefulWidget {
  @override
  _LoginHerokuState createState() => _LoginHerokuState();
}

class _LoginHerokuState extends State<LoginHeroku> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pink[300],
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Text(
                  'H E R O K U',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, color: Colors.white, fontSize: 88),
                ),
               ),
            ),
            SizedBox(height: 95),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(borderSide: const BorderSide(color: Colors.white, width: 3.0),),
                    labelText: 'Email',
                    hintText: ' Masukkan alamat email yang valid, misalnya xxx@gmail.com'),
                    style: TextStyle(fontSize: 15.0, color: Colors.white),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(borderSide: const BorderSide(color: Colors.white, width: 3.0),),
                    labelText: 'Sandi',
                    hintText: ' Masukkan sandi yang benar-benar aman'),
                    style: TextStyle(fontSize: 15.0, color: Colors.white),
              ),
            ),
            SizedBox(height: 40),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.purple[500], borderRadius: BorderRadius.circular(30)),
              child: FlatButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => HomePage()));
                },
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.black, fontSize: 25),
                ),
              ),
            ),
            SizedBox(height: 37),
            FlatButton(
              onPressed: (){
              },
              child: Text(
                'Lupa sandi? Klik disini',
                style: TextStyle(color: Colors.black, fontSize: 20),
              ),
            ),
            SizedBox(height: 50),
            Text('Selamat datang! Silahkan buat akun Anda')
          ],
        ),
      ),
    );
  }
}
