from django.urls import path
from .views import message_info, index, xml, json, add_note, note_list

urlpatterns = [
    path('', index, name='index'),
    path('MESSAGE', message_info, name='message_info'),
    path('', xml, name = '/xml'),
    path('', json, name = '/json'),
    path('add-note', add_note, name='add_note'),
    path('note-list', note_list, name='note_List')
]












