from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from datetime import datetime, date
from .models import Note
from .forms import NoteForm



sender_name = input('Insert the name of the message sender : ' , )  
receiver_name = input('Insert the name of the message receiver : ' , )
title = input('Insert the title of your message : ' , )
message = input('Type your message here : ' , )



def index(request):
    response = {'sender': sender_name,
                'receiver': receiver_name,
                'title' : title,
                'message' : message
                }
    return render(request, 'lab4._index.html', response)

def message_info(request):
    messages = Note.objects.all().values() # TODO Implement this
    # $$$ #
    response = {'NOTE': messages}
    return render(request, 'NOTE_lab4._index.html', response)

def add_note(request) :
    context ={}
  
    form = NoteForm(request.POST or None, request.FILES or None)
      
    if form.is_valid():
        form.save()
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request) :
    response = {'sender': sender_name,
                'receiver': receiver_name,
                'title' : title,
                'message' : message
                }
    return render(request, 'lab4_note_list.html', response)


def xml() :
    messages = Note.objects.all().values()
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json() :
    messages = Note.objects.all().values()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
