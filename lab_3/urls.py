from django.urls import path
from .views import friend_list, index, add_friend

urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list, name='friend_list'),
    path('add', add_friend, name='add_friend')
    # TODO Add friends path using friend_list Views
]