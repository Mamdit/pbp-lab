import 'package:flutter/material.dart';

class AudioWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          IconButton(icon: Icon(Icons.play_arrow)),
          Text('00:00'),
          Slider(value: 0),
          Text('00:59'),
        ],
      ),
    );
  }
}