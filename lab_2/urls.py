from django.urls import path
from .views import message_info, index, xml, json

urlpatterns = [
    path('', index, name='index'),
    path('MESSAGE', message_info, name='Message Information'),
    path('', xml, name = '/xml'),
    path('', json, name = '/json')
    # TODO Add friends path using friend_list Views
]
