from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from datetime import datetime, date
from .models import Note

def index(request):
    messages = Note.objects.all().values() 
    response = {'Notes': messages}
    return render(request, 'lab5_index.html', response)

def xml() :
    messages = Note.objects.all().values()
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json() :
    messages = Note.objects.all().values()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
